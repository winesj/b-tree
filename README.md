# B-tree

A [B-tree](https://en.wikipedia.org/wiki/B-tree) is a neat self-balancing tree often used in databases.

To build:
* `nix build` or if you don't wish to use nix:
* [Install cabal & ghc](https://haskell.org/ghcup) if you don't have them.
* `cabal build`
