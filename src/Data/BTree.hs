module Data.BTree where
import qualified Data.List.NonEmpty as NE
import qualified Data.Foldable as F
import qualified Data.Maybe as My

defaultOrder :: Word
defaultOrder = 5

data Tree a = Tree {
  order :: Word,
  headNode :: Node a
                 } deriving (Show)

-- Every node has at most m children.
-- Every internal node has at least ⌈m/2⌉ children.
-- Every non-leaf node has at least two children.
-- All leaves appear on the same level.
-- A non-leaf node with k children contains k−1 keys.
data Node a = Leaf | Node {
  leftMostNode :: Node a,
  rest :: NE.NonEmpty (a, Node a)
                          } deriving (Eq, Ord, Show)

withLeafChildren :: NE.NonEmpty a -> Node a
withLeafChildren = Node Leaf . NE.map (, Leaf)

singleton :: a -> Node a
singleton a = Node Leaf (NE.singleton (a, Leaf))

data NodeInsertionResult a = Inserted (Node a) | Split (Node a) a (Node a)

empty :: Tree a
empty = Tree defaultOrder Leaf

fromList :: Ord a => [a] -> Tree a
fromList = foldl (flip insert) empty

toList :: Tree a -> [a]
toList = toList' . headNode
  where
    toList' :: Node a -> [a]
    toList' (Node {..}) = (++) (toList' leftMostNode) . concatMap (\(pivot, n) -> pivot : toList' n) . NE.toList $ rest
    toList' Leaf = []

insert :: forall a. Ord a => a -> Tree a -> Tree a
insert x Tree {headNode = Leaf, ..} = Tree {headNode = singleton x, ..}
insert x (Tree {..}) = Tree order $ case insert' headNode of
  Inserted n -> n
  Split n center n' -> Node n $ NE.singleton (center, n')
  where
    insert' :: Node a -> NodeInsertionResult a
    insert' Leaf = Split Leaf x Leaf
    insert' (Node {..}) = splitNodeIfNeeded order $ case NE.span ((< x) . fst) rest of
      ([], _) -> case insert' leftMostNode of
        Inserted n -> Node n rest
        Split n center n' -> Node n (NE.cons (center, n') rest)
      (prev, post) -> case insert' . snd . last $ prev of
        Inserted n -> Node leftMostNode $ NE.appendList (NE.prependList (init prev) (NE.singleton . (,n) . fst . last $ prev)) post
        Split n center n' -> Node leftMostNode $ NE.prependList (init prev) (((,n) . fst . last $ prev) NE.<| [(center, n')])

toNode :: NE.NonEmpty (Node a) -> NE.NonEmpty a -> Node a
toNode children pivots = Node (NE.head children) (NE.zip pivots (NE.fromList . NE.tail $ children))

splitNodeIfNeeded :: Word -> Node a -> NodeInsertionResult a
splitNodeIfNeeded order n
  | fromIntegral order <= (NE.length . rest $ n) = splitNode n
  | otherwise = Inserted n

splitNode :: Node a -> NodeInsertionResult a
splitNode Leaf = Inserted Leaf
splitNode (Node {..}) = Split leftSplit middle rightSplit
  where
    leftSplit = Node leftMostNode (NE.fromList $ NE.take halfRestLength rest)

    rightNodes = NE.fromList . NE.drop halfRestLength $ rest
    rightSplit = Node (snd . NE.head $ rightNodes) (NE.fromList . NE.tail $ rightNodes)

    middle = fst . NE.head $ rightNodes

    halfRestLength = (`div` 2) . NE.length $ rest

member :: forall a. Ord a => a -> Tree a -> Bool
member x (Tree {..}) = member' headNode
  where
    member' :: Node a -> Bool
    member' Leaf = False
    member' (Node {..}) = match == x || member' childNode
      where
        match :: a
        childNode :: Node a
        (match, childNode) = My.fromMaybe (fst . NE.head $ rest, leftMostNode) . F.find ((>= x) . fst) $ rest
